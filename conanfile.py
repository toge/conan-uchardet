from conans import ConanFile, CMake, tools
import os

class UchardetConan(ConanFile):
    name = "uchardet"
    license = "MPL-1.1"
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-uchardet/"
    homepage = "https://gitlab.freedesktop.org/uchardet/uchardet"
    description = "An encoding detector library ported from Mozilla"
    topics = ("mozilla", "encoding", "freedesktop")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake"

    def _cmake(self):
        cmake = CMake(self)
        cmake.definitions["BUILD_BINARY"] = False
        cmake.definitions["BUILD_STATIC"] = not self.options.shared
        cmake.configure(source_folder="uchardet")
        return cmake

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        tools.get(**self.conan_data["sources"][self.version])
        os.rename("uchardet-{}".format(self.version), "uchardet")

    def build(self):
        cmake = self._cmake()
        cmake.build()

    def package(self):
        cmake = self._cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["uchardet"]

