#include <iostream>

#include "uchardet/uchardet.h"

int main() {
    uchardet_t handle = uchardet_new();

    char const DATA[] = "Hello World.";

    uchardet_handle_data(handle, DATA, sizeof(DATA));

    uchardet_delete(handle);

    char const* charset = uchardet_get_charset(handle);

    if (charset[0]) {
        std::cout << charset << std::endl;
    } else {
        std::cout << "unknown" << std::endl;
    }

    return 0;
}
